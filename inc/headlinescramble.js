// source: http://snippets.dzone.com/posts/show/849
function shuffle(o) {
  for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
}
function pick(thearray) {
  return thearray[Math.floor(Math.random() * thearray.length)];
}
function init() {
  var divs = $$('div.headline');
  do_headline(pick(divs).innerHTML);
}
function make_word (word) {
  var elt = document.createElement("span");
  elt.className = "word";
  elt.innerHTML = word;
  return elt;
}
var words = [];
function make_swapbutton (index) {
  var swap = document.createElement("a");
  swap.className="swapbutton";
  swap.innerHTML = " &lt;&gt ";
  swap.href = "javascript:doswap("+index+")";
  return swap;
}
function swap_elements (elt1, elt2) {
  // helpful: http://developer.mozilla.org/en/docs/DOM:element
  // we need to use: parentNode, nextSibling, removeChild, insertBefore 
  var next1 = elt1.nextSibling;
  var next2 = elt2.nextSibling;
  // need to store references to the parents, since they become null once removeChild is called
  var parent1 = elt1.parentNode;
  var parent2 = elt2.parentNode;
  // REMOVE THE ELEMENTS (via their parents)
  parent1.removeChild(elt1);
  parent2.removeChild(elt2);
  // REPLACE THE OTHER ELEMENTS
  if (next1) {
    parent1.insertBefore(elt2, next1);
  } else {
    parent1.appendChild(elt2);
  }

  if (next2) {
    parent2.insertBefore(elt1, next2);
  } else {
    parent2.appendChild(elt1);
  }
}
function doswap(index) {
  var w1 = words[index];
  var w2 = words[index+1];
  // swap the HTML elements
  swap_elements(w1, w2);
  // swap in the words array too (to keep in sync with HTML!)
  words[index] = w2;
  words[index+1] = w1;
}
function check() {
  var userphrase = "";
  for (var i=0; i<words.length; i++) {
    userphrase += words[i].innerHTML;
    if (i+1 < words.length) userphrase += " ";
  }
  // EXTEND THIS TO CHECK userphrase against originalphrase
  // & give feedback on how close (or not) the user is!
  // for now just give the answer!
  alert(originalphrase);
  console.log("ORIGINAL:", originalphrase);
  console.log("USER:", userphrase);
}
var originalphrase = "";
function do_headline(headline) {
  // SPLIT THE HEADLINE AND SOURCE (NEWS AGENCY)
  // ie make "Obama wins in South Carolina - Reuters" => ["Obama wins...", "Reuters"]
  var parts = headline.split(/\s-\s/, 2);
  // SPLIT the words on whitespace (\s)
  // ie turn "Obama wins in South Carolina" => ["Obama", "wins", "in", ...]
  var thewords = parts[0].split(/\s/);
  // store the source (we can show it as a "hint" to the user)
  var thesrc = parts[1];
  $('source').innerHTML = "Source: " + thesrc;
  // debug: check what we got
  console.log("got", thewords, thesrc);
  // STORE AWAY THE ORIGINAL (to check for WIN)
  originalphrase = thewords.join(" ").toLowerCase();
  // use our shuffle function to scramble the words
  thewords = shuffle(thewords);
  // LOOP OVER each word
  for (var i=0; i<thewords.length; i++) {
    // nb: convert to lower case to make the game harder!
    var elt = make_word(thewords[i].toLowerCase());
    elt.id = "word"+i;
    $('words').appendChild(elt);
    words.push(elt);
    // MAKE SWAP BUTTON
    if (i+1<thewords.length) {
      var swap = make_swapbutton(i);
      $('words').appendChild(swap);
    }
  }
  
}
